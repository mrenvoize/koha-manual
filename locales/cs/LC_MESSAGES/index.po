# Compendium of cs.
msgid ""
msgstr ""
"Project-Id-Version: compendium-cs\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-05-25 17:20-0300\n"
"PO-Revision-Date: 2021-05-25 20:24-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/index.rst:3
msgid "Koha |release| Manual (en)"
msgstr "Manuál na Koha |release| (cs)"

#: ../../source/index.rst:5
msgid "Author: The Koha Community"
msgstr "Author: Česká komunita Koha"
