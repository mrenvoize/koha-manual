# Compendium of zh_TW.
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-12 07:29-0300\n"
"PO-Revision-Date: 2022-07-06 04:01+0000\n"
"Last-Translator: jane <a230591160@gmail.com>\n"
"Language-Team: Koha Translation Team\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1657080112.244594\n"
"X-Pootle-Path: /zh_TW/manual22.11/webservices.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/webservices.rst:6
#, fuzzy
msgid "Web services"
msgstr "網頁服務"

#: ../../source/webservices.rst:8
msgid ""
"Koha provides a number of APIs allowing access to it's data and functions."
msgstr "Koha 提供若干 API 近用其資料與功能。"

#: ../../source/webservices.rst:13
msgid "OAI-PMH"
msgstr "OAI-PMH"

#: ../../source/webservices.rst:15
msgid ""
"For the Open Archives Initiative-Protocol for Metadata Harvesting (OAI-PMH) "
"there are two groups of 'participants': Data Providers and Service "
"Providers. Data Providers (open archives, repositories) provide free access "
"to metadata, and may, but do not necessarily, offer free access to full "
"texts or other resources. OAI-PMH provides an easy to implement, low barrier "
"solution for Data Providers. Service Providers use the OAI interfaces of the "
"Data Providers to harvest and store metadata. Note that this means that "
"there are no live search requests to the Data Providers; rather, services "
"are based on the harvested data via OAI-PMH."
msgstr ""
"開放檔案存取協定 (OAI-PMH) 有兩組 '參與者'：資料提供者與服務提供者。資料提供"
"者 (開放檔案、典藏庫) 提供自由近用的後設資料，以及可能，但不是必要的，自由近"
"用全文或其他資源。OAI-PMH 提供簡便、低門檻的解決方案給資料提供者。服務提供者"
"運用資料提供者的 OAI 介面擷取並儲存後設資料。沒有向資料提供者提出即時搜尋要"
"求；而是，以OAI-PMH 擷取資料。"

#: ../../source/webservices.rst:26
#, fuzzy
msgid ""
"Koha at present can only act as a Data Provider. It can not harvest from "
"other repositories. The biggest stumbling block to having Koha harvest from "
"other repositories is that MARC is the only metadata format that Koha "
"indexes natively."
msgstr ""
"目前 Koha 祗能擔任資料提供者的角色。不能從其他典藏庫提取資料。主要的障礙在於 "
"MARC 祗能供 Koha 索引使用。見 http://www.oaforum.org/tutorial/english/page3."
"htm 有關 OAI-PMH 運作的圖示。"

#: ../../source/webservices.rst:31
#, fuzzy
msgid ""
"By default Koha won't include item information in OAI-PMH result sets, but "
"they can be added by using the include_items option in the a configuration "
"file linked from :ref:`OAI-PMH:ConfFile <oai-pmh-conffile-label>`."
msgstr ""
"Koha 預設不在 OAI-PMH 結果內包括館藏資訊，但可以用 :ref:`OAI-PMH:ConfFile`偏"
"好內的 include_items 組態把它加入。"

#: ../../source/webservices.rst:35
msgid "Learn more about OAI-PMH at: http://www.openarchives.org/pmh/"
msgstr "OAI-PMH 詳情見：http://www.openarchives.org/pmh/"

#: ../../source/webservices.rst:37
#, fuzzy
msgid ""
"To enable OAI-PMH in Koha edit the :ref:`OAI-PMH <oai-pmh-pref-label>` "
"preference. Once enabled you can visit http://YOURKOHACATALOG/cgi-bin/koha/"
"oai.pl to see your file."
msgstr ""
"編輯 :ref:`OAI-PMH` 偏好啟用 OAI-PMH。啟用之後就能進入 http://"
"YOURKOHACATALOG/cgi-bin/koha/oai.pl 查看檔案。"

#: ../../source/webservices.rst:44
#, fuzzy
msgid "Sample OAI conf file"
msgstr "OAI 組態檔範例"

#: ../../source/webservices.rst:68
msgid "SRU server"
msgstr "SRU 伺服器"

#: ../../source/webservices.rst:70
msgid ""
"Koha implements the Search/Retrieve via URL (SRU) protocol. More information "
"about the protocol itself can be found at http://www.loc.gov/standards/sru/. "
"The version implemented is version 1.1."
msgstr ""
"Koha 經由 URL (SRU) 通信協定執行搜尋/取得作業。詳情請參見 http://www.loc.gov/"
"standards/sru/。使用1.1 版。"

#: ../../source/webservices.rst:78
msgid "Explain"
msgstr "解釋"

#: ../../source/webservices.rst:80
msgid ""
"If you want to have information about the implementation of SRU on a given "
"server, you should have access to the Explain file using a request to the "
"server without any parameter. Like http://myserver.com:9999/biblios/. The "
"response from the server is an XML file that should look like the following "
"and will give you information about the default settings of the SRU server."
msgstr ""
"查看指定伺服器應用 SRU 的資訊，您應以無參數的方式近用至伺服器的 Explain 檔"
"案。如：http://myserver.com:9999/biblios/。回應的資料是 XML 格式看來如下且提"
"供 SRU 伺服器的預設設定資訊。"

#: ../../source/webservices.rst:148
msgid "Search"
msgstr "搜尋"

#: ../../source/webservices.rst:150
msgid ""
"This url : http://myserver.com:9999/biblios?"
"version=1.1&operation=searchRetrieve&query=reefs is composed of the "
"following elements:"
msgstr ""
"此網址：http://myserver.com:9999/biblios?version=1.1&amp;"
"operation=searchRetrieve&amp;query=reefs 由下列元素構成："

#: ../../source/webservices.rst:154
msgid "base url of the SRU server : http://myserver.com:9999/biblios?"
msgstr "SRU 伺服器的網址是：http://myserver.com:9999/biblios?"

#: ../../source/webservices.rst:156
msgid ""
"search part with the 3 required parameters : version, operation and query. "
"The parameters within the search part should be of the key=value form, and "
"can be combined with the & character."
msgstr ""
"搜尋部份有 3 個必備參數：版本、運算元與詢問內容。這些參數應以 key=value 的格"
"式呈現，且可以 &amp; 字元連結。"

#: ../../source/webservices.rst:160
msgid ""
"One can add optional parameters to the query, for instance maximumRecords "
"indicating the maximum number of records to be returned by the server. So "
"http://myserver.com:9999/biblios?"
"version=1.1&operation=searchRetrieve&query=reefs&maximumRecords=5 will only "
"get the first 5 results results from the server."
msgstr ""
"此詢問可以新增參數，如：maximumRecords 表示從該伺服器送回的記錄極限。所以"
"http://myserver.com:9999/biblios?version=1.1&amp;"
"operation=searchRetrieve&amp;query=reefs&amp;maximumRecords=5 祗能從伺服器取"
"得最多 5 筆結果。"

#: ../../source/webservices.rst:166
msgid "The \"operation\" key can take two values: scan or searchRetrieve."
msgstr "在 \"運算元\" 鍵有兩個值： scan 或 searchRetrieve。"

#: ../../source/webservices.rst:168
msgid ""
"If operation=searchRetrieve, then the search key should be query. As in : "
"operation=searchRetrieve&query=reefs"
msgstr ""
"若 operation=searchRetrieve，則搜尋鍵詞應為詢問。如："
"operation=searchRetrieve&amp;query=reefs"

#: ../../source/webservices.rst:171
msgid ""
"If operation=scan, then the search key should be scanClause. As in : "
"operation=scan&scanClause=reefs"
msgstr ""
"若 operation=scan，則搜尋鍵詞應為 scanClause。如：operation=scan&amp;"
"scanClause=reefs"

#: ../../source/webservices.rst:174
msgid ""
"etc/zebradb/biblios/etc/bib1.att defines Zebra/3950 indexes that exist on "
"your system. For instance you'll see that we have indexes for Subject and "
"for Title: att 21 Subject and att 4 Title respectively."
msgstr ""
"etc/zebradb/biblios/etc/bib1.att 定義系統內存在的 Zebra/3950 索引。如：可看到"
"對主題與題名的索引分別是：att 21 Subject 與 att 4 Title。"

#: ../../source/webservices.rst:178
msgid ""
"In the pqf.properties file located under etc/zebradb/pqf.properties I see "
"that an access point already uses my Subject index (index.dc.subject = 1=21) "
"while another uses my Title index (index.dc.title = 1=4) I know this is my "
"Subject index because as I've seen just before in my bib1.att file, it's "
"called with =1=21 in Z3950: so index.dc.subject = 1=21 correctly points to "
"my Subject index. And Title was called with 1=4 so index.dc.title = 1=4 "
"correctly points to my Title index. I can now construct my query just like I "
"would in a search box, just preceding it with the \"query\" key: "
"query=Subject=reefs and Title=coral searches \"reefs\" in the subject and "
"\"coral\" in the title. The full url would be http://myserver.com:9999/"
"biblios?version=1.1&operation=searchRetrieve&query=Subject=reefs and "
"Title=coral If I want to limit the result set to just 5 records, I can do "
"http://myserver.com:9999/biblios?"
"version=1.1&operation=searchRetrieve&query=Subject=reefs and "
"Title=coral&maximumRecords=5"
msgstr ""
"當另個使用者使用我的題名索引 (index.dc.title = 1=4) 就可看到位在 etc/zebradb/"
"pqf.properties 的 pqf.properties 檔案已使用我的主題索引 (index.dc.subject = "
"1=21) 我知道這個是我的主題索引因為在我的 bib1.att 檔案裡見過它，稱為 with "
"=1=21 於 Z3950: 所以 index.dc.subject = 1=21 精確指向我的主題索引。且題名為 "
"1=4 所以 index.dc.title = 1=4 精確指向題名索引。我可以像在搜尋盒裡一樣建構自"
"己的搜尋，祗要加入 \"query\" 鍵：query=Subject=reefs and Title=coral 在主題內"
"搜尋 \"reefs\" 且在題名內搜尋 \"coral\"。完整的網址像是這樣 http://myserver."
"com:9999/biblios?version=1.1&amp;operation=searchRetrieve&amp;"
"query=Subject=reefs and Title=coral 若我要限制祗呈現 5 筆記錄，則為 http://"
"myserver.com:9999/biblios?version=1.1&amp;operation=searchRetrieve&amp;"
"query=Subject=reefs and Title=coral&amp;maximumRecords=5"

#: ../../source/webservices.rst:194
msgid ""
"I can also play with truncate, relations, etc. Those are also defined in my "
"pqf.properties file. I can see for instance the position properties defined "
"as:"
msgstr ""
"我也可使用截切、相關等用法。它們都定義在我的 pqf.properties 檔案內。我可看到"
"其用法如："

#: ../../source/webservices.rst:205
msgid ""
"So as an example if I want \"coral\" to be at the beginning of the title, I "
"can do this query : http://myserver.com:9999/biblios?"
"version=1.1&operation=searchRetrieve&query=Title=coral first"
msgstr ""
"所以，舉例來說我想要找尋題名起頭為 \"coral\" 的紀錄，我可以做這樣的搜尋："
"http://myserver.com:9999/biblios?version=1.1&amp;"
"operation=searchRetrieve&amp;query=Title=coral first"

#: ../../source/webservices.rst:213
msgid "Retrieve"
msgstr "取得搜尋結果"

#: ../../source/webservices.rst:215
msgid ""
"My search for http://univ\\_lyon3.biblibre.com:9999/biblios?"
"version=1.1&operation=searchRetrieve&query=coral reefs&maximumRecords=1 "
"retrieves just on record. The response looks like this:"
msgstr ""
"我的搜尋 http://univ\\_lyon3.biblibre.com:9999/biblios?version=1.1&amp;"
"operation=searchRetrieve&amp;query=coral reefs&amp;maximumRecords=1 提取一筆"
"紀錄。回應的內容像這樣："

#: ../../source/webservices.rst:286
msgid "ILS-DI"
msgstr "ILS-DI"

#: ../../source/webservices.rst:288
#, fuzzy
msgid ""
"As of writing, the self documenting ILS-DI is the most complete interface "
"and after it has been enabled as described in the :ref:`ILS-DI system "
"preferences <webservices-ils-di-system-preferences-label>` section. The "
"documentation should be available at https://YOURKOHACATALOG/cgi-bin/koha/"
"ilsdi.pl"
msgstr ""
"撰寫之後，自我生成的 ILS-DI 就是完整的介面，經由 :ref:`ils-di-label`系統偏好"
"的設定後，該文件可以從 https://YOURKOHACATALOG/cgi-bin/koha/ilsdi.pl 取得"

#: ../../source/webservices.rst:297
msgid "JSON reports services"
msgstr "JSON 報表服務"

#: ../../source/webservices.rst:299
#, fuzzy
msgid ""
"Koha implements a JSON reports service for every report saved using the :ref:"
"`Guided reports wizard <guided-report-wizard-label>` or :ref:`Report from "
"SQL <report-from-sql-label>` features."
msgstr ""
"Koha 把 JSON 報表服務應用在儲存於 :ref:`報表精靈 <guided-report-wizard-"
"label>` 或 :ref:`來自 SQL 的報表 <report-from-sql-label>` 功能中。"

#: ../../source/webservices.rst:303
msgid ""
"By default reports will be non-public and only accessible by authenticated "
"users. If a report is explicitly set to *public* it will be accessible "
"without authentication by anyone. This feature should only be used when the "
"data can be shared safely not containing any patron information."
msgstr ""
"報表預設為不公開祗有被授權的使用者才能看到。若揭示為 *公開* 則任何人均可看"
"到。祗有不含個人資訊的報表，才可以公開。"

#: ../../source/webservices.rst:308
msgid "The reports can be accessed using the following URLs:"
msgstr "經由以下的 URL 近用該等報表："

#: ../../source/webservices.rst:310
msgid "Public reports"
msgstr "公開的報表"

#: ../../source/webservices.rst:312
msgid "OpacBaseURL/cgi-bin/koha/svc/report?id=REPORTID"
msgstr "OpacBaseURL/cgi-bin/koha/svc/report?id=REPORTID"

#: ../../source/webservices.rst:314
msgid "Non-public reports"
msgstr "不公開的報表"

#: ../../source/webservices.rst:316
msgid "StaffBaseURL/cgi-bin/koha/svc/report?id=REPORTID"
msgstr "StaffBaseURL/cgi-bin/koha/svc/report?id=REPORTID"

#: ../../source/webservices.rst:318
msgid "There are also some additional parameters available:"
msgstr "還有其他可用的參數："

#: ../../source/webservices.rst:320
msgid ""
"Instead of accessing the report by REPORTID you can also use the report's "
"name:"
msgstr "以 REPORTID 近用報表之外，還可以用報表名稱近用它們："

#: ../../source/webservices.rst:323
msgid ".../cgi-bin/koha/svc/report?name=REPORTNAME"
msgstr ".../cgi-bin/koha/svc/report?name=REPORTNAME"

#: ../../source/webservices.rst:325
msgid ""
"For easier development there is also an option to generate an annotated "
"output of the data. It will generate an array of hashes that include the "
"field names as keys."
msgstr "還有簡易的方法產生資料。產生以欄位名稱為鍵詞的陣列。"

#: ../../source/webservices.rst:329
msgid ".../cgi-bin/koha/svc/report?name=REPORTNAME&annotated=1"
msgstr ".../cgi-bin/koha/svc/report?name=REPORTNAME&annotated=1"

#: ../../source/webservices.rst:334
#, fuzzy
msgid "Versioned RESTful API effort"
msgstr "可視的 RESTful API Effort"

#: ../../source/webservices.rst:336
msgid ""
"There is an ongoing effort to converge the APIs above into a single "
"versioned set of modern RESTful endpoints documented using the OpenAPI "
"standard and available by default under https://YOURKOHACATALOG/api/v1/"
msgstr ""
"以 OpenAPI 標準轉換前述的 API 至現代的 RESTful 文件，預設的位置為 https://"
"YOURKOHACATALOG/api/v1/"

#: ../../source/webservices.rst:343
msgid "OAuth2 client credentials grant"
msgstr "授與 OAuth2 客戶端認證"

#: ../../source/webservices.rst:345
msgid ""
"Koha supports the OAuth2 client credentials grant as a means to secure the "
"API for using it from other systems to adhere to current industry standards. "
"More information on the OAuth2 client credentials grant standard `can be "
"found here <https://auth0.com/docs/api-auth/grant/client-credentials>`_."
msgstr ""
"Koha 支援授與 OAuth2 客戶端認證做為 API 安全機制。詳情見 `可在此找到 "
"<https://auth0.com/docs/api-auth/grant/client-credentials>`_。"

#: ../../source/webservices.rst:351
msgid "API key management interface for patrons"
msgstr "讀者的 API 金鑰管理介面"

#: ../../source/webservices.rst:353
#, fuzzy
msgid ""
"In order for API keys to be create for patrons, the system preference :ref:"
"`RESTOAuth2ClientCredentials <restoauth2clientcredentials-label>` **must** "
"be enabled for the option to appear in a patron record."
msgstr ""
"為了讓讀者新增 API 金鑰，系統偏好 :ref:`RESTOAuth2ClientCredentials` **必須"
"** 啟用才能在讀者紀錄內顯示該選項。"

#: ../../source/webservices.rst:357
msgid "Navigate to a patron record and select *More > Manage API keys*"
msgstr "瀏覽讀者紀錄並選擇 *更多 > 管理 API 金鑰*"

#: ../../source/webservices.rst:359
msgid "|image1336|"
msgstr "|image1336|"

#: ../../source/webservices.rst:361
msgid ""
"If no API keys exist for a patron there will be a message prompting to "
"generate a client id/secret pair"
msgstr "若讀者沒有 API 金鑰，則顯示需要客戶 id/secret 配對的訊息"

#: ../../source/webservices.rst:364
msgid "|image1337|"
msgstr "|image1337|"

#: ../../source/webservices.rst:366
msgid "Enter a description for the client id/secret pair and click Save"
msgstr "鍵入客戶 id/secret 配對說明並按儲存"

#: ../../source/webservices.rst:368
msgid "|image1338|"
msgstr "|image1338|"

#: ../../source/webservices.rst:370
msgid ""
"Koha will generate a client id/secret pair for use to connect to Koha from "
"other third-party systems as an authenticated client"
msgstr "koha 將產生客戶 id/secret 配對做為連結第三方系統的認證基礎"

#: ../../source/webservices.rst:373
msgid "|image1339|"
msgstr "|image1339|"

#: ../../source/webservices.rst:375
msgid ""
"Clicking the Revoke button next to an API credential pair will render the "
"specific credential pair inactive until reactivated"
msgstr "按 API 認證配對旁的撤銷鈕，將停用指定定配對認證直到再次啟用它"

#: ../../source/webservices.rst:381
msgid "Barcode image generator"
msgstr "條碼影像產生器"

#: ../../source/webservices.rst:383
msgid ""
"Koha provides a barcode image generator on both the staff interface and the "
"public interface. Both require a user to be logged in to use the service, to "
"prevent abuse by third parties."
msgstr ""
"Koha 在館員介面和公共介面上都提供了條碼影像產生器。兩者都需要用戶登錄才能使用"
"該服務，以防止第三方濫用。"

#: ../../source/webservices.rst:386 ../../source/webservices.rst:408
msgid "For example::"
msgstr "範例::"

#: ../../source/webservices.rst:386
msgid "/cgi-bin/koha/svc/barcode?barcode=123456789&type=UPCE"
msgstr "/cgi-bin/koha/svc/barcode?barcode=123456789&type=UPCE"

#: ../../source/webservices.rst:388
msgid ""
"The URL above will generate a barcode image for the barcode \"123456789\", "
"using the UPCE barcode format."
msgstr "上面的 URL 將使用 UPCE 條碼格式為條碼“123456789”生成條碼影像。"

#: ../../source/webservices.rst:390
msgid ""
"The available barcode types are: * Code39 * UPCE * UPCA * QRcode * NW7 * "
"Matrix2of5 * ITF * Industrial2of5 * IATA2of5 * EAN8 * EAN13 * COOP2of5"
msgstr ""
"可用的條碼類型有：* Code39 * UPCE * UPCA * QRcode * NW7 * Matrix2of5 * ITF * "
"Industrial2of5 * IATA2of5 * EAN8 * EAN13 * COOP2of5"

#: ../../source/webservices.rst:404
msgid "If no type is specified, Code39 will be used."
msgstr "如果未指定類型，則將使用 Code39。"

#: ../../source/webservices.rst:406
msgid ""
"By default, the barcode image will contain the text of the barcode as well. "
"If this is not desired, the parameter \"notext\" may be passed to supress "
"this behavior."
msgstr ""
"預設情況下，條碼影像也將包含條碼的文字。如果不需要，可以傳遞參數“notext”來抑"
"制這種行為。"

#: ../../source/webservices.rst:412
msgid ""
"will generate a barcode image 123456789 without the text \"123456789\" below "
"it."
msgstr "將產生一個條碼影像 123456789，下面沒有文字“123456789”。"

#: ../../source/webservices.rst:414
msgid ""
"This service can be used to embed barcode images into slips and notices "
"printed from the browser, and to embed a patron cardnumber into the OPAC, "
"among other possibilities."
msgstr ""
"該服務可用於將條碼影像嵌入從瀏覽器打印的單據和通知中，以及將顧客卡號嵌入 "
"OPAC 中，以及其他可能性。"

#~ msgid ""
#~ "http://www.loc.gov/standards/sru/sru1-1archive/search-retrieve-operation."
#~ "html gives more details about the search operations and in particular the "
#~ "list of optional parameters for searching."
#~ msgstr ""
#~ "http://www.loc.gov/standards/sru/sru1-1archive/search-retrieve-operation."
#~ "html 提供有關搜尋作業更多的詳情特別是搜尋用的參數清單。"

#~ msgid "REST services"
#~ msgstr "REST 服務"

#~ msgid ""
#~ "Koha can now be requested by REST http requests. BibLibre wrotes an "
#~ "external module to adds more possibilities than ILS-DI can provide.There "
#~ "is no authentication process, but authorized ips are listed in the config "
#~ "file. Services have been tested in 3.10, 3.12 and 3.14 koha versions. You "
#~ "can find more information about it into README file and opac/rest.pl "
#~ "documentation on http://git.biblibre.com. If you want to add features, "
#~ "send us a patch at dev\\_patches AT biblibre DOT com."
#~ msgstr ""
#~ "可以經由 REST http 請求向 Koha 提出請求。BibLibre 公司撰寫一個外部模組，增"
#~ "加 ILS-DI 所無的服務。不需認證程序，但可把認證後的 ip 列在組態檔內。該等服"
#~ "務在 Koha 3.10、3.12 與 3.14 版均被測試過。詳情在 http://git.biblibre.com "
#~ "的 README 檔案與 opac/rest.pl。若需額外的功能，請洽 dev\\_patches AT "
#~ "biblibre DOT com。"

#~ msgid "Services provided in 1.4 version are:"
#~ msgstr "1.4 版提供的服務是："
