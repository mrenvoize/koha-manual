# Italian translations for Koha Manual package.
# Copyright (C) 2020, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# Automatically generated, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-12 07:29-0300\n"
"PO-Revision-Date: 2021-03-01 20:19-0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Pootle-Path: /it/manual22.11/cash_management.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/cash_management.rst:6
msgid "Point of sale"
msgstr ""

#: ../../source/cash_management.rst:8
msgid ""
"Point of sale is a module designed for selling items to people who are not "
"registered at the library or to make sales that do not need to be linked to "
"a patron account."
msgstr ""

#: ../../source/cash_management.rst:12
msgid ""
"For example, you can sell used books or promotional items. These items can "
"be sold to anyone and you don't need to link the sale to a particular patron."
msgstr ""

#: ../../source/cash_management.rst:15
msgid ""
"For invoices that need to be linked to a patron's account (like a lost item "
"or new card fee), use :ref:`manual invoicing <creating-manual-invoices-"
"label>`."
msgstr ""

#: ../../source/cash_management.rst:18
msgid "*Get there:* More > Point of sale"
msgstr ""

#: ../../source/cash_management.rst:23
msgid "Setup"
msgstr ""

#: ../../source/cash_management.rst:25
msgid ""
"To enable the point of sale module, you must turn on the :ref:"
"`EnablePointOfSale <EnablePointOfSale-label>` system preference."
msgstr ""

#: ../../source/cash_management.rst:28
msgid ""
"If it's not already done, you must also enable the :ref:`UseCashRegisters "
"<UseCashRegisters-label>` system preference."
msgstr ""

#: ../../source/cash_management.rst:31
msgid ""
"Make sure you :ref:`configure your cash registers <cashregisters-label>` in "
"the administration module."
msgstr ""

#: ../../source/cash_management.rst:34
msgid ""
"Finally, you must add items that you will be selling in the :ref:`debit "
"types <debit-types-label>` section of the administration module."
msgstr ""

#: ../../source/cash_management.rst:40
msgid "Making a sale"
msgstr ""

#: ../../source/cash_management.rst:42
msgid ""
"When you first go in the point of sale module, the left side will show all "
"the items for sale. These are :ref:`debit types <debit-types-label>` that "
"were marked as 'Can be sold'."
msgstr ""

#: ../../source/cash_management.rst:46
msgid "**Note**"
msgstr ""

#: ../../source/cash_management.rst:48
msgid ""
"You can customize the columns of this table in the :ref:`'Table "
"settings'<column-settings-label>` section of the Administration module "
"(table id: invoices)."
msgstr ""

#: ../../source/cash_management.rst:52
msgid "On the right side is the current sale."
msgstr ""

#: ../../source/cash_management.rst:54
msgid "|image1497|"
msgstr "|image1497|"

#: ../../source/cash_management.rst:56
msgid "Click on the 'Add' button next to the items to add to the current sale."
msgstr ""

#: ../../source/cash_management.rst:58
msgid ""
"If you need to change the cost or the quantity, click on the amount on the "
"right side and it will become an input box where you can enter the correct "
"amount."
msgstr ""

#: ../../source/cash_management.rst:62
msgid "|image1459|"
msgstr "|image1459|"

#~ msgid "|image1515|"
#~ msgstr "|image1515|"
