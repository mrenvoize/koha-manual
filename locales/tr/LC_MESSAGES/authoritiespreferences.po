# Turkish translations for Koha Manual package.
# Copyright (C) 2020, Koha Community
# This file is distributed under the same license as the Koha Manual package.
# Automatically generated, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-12 07:29-0300\n"
"PO-Revision-Date: 2021-03-01 20:19-0300\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Pootle-Path: /tr/manual22.11/authoritiespreferences.po\n"
"X-Pootle-Revision: 1\n"

#: ../../source/authoritiespreferences.rst:6
msgid "Authorities"
msgstr ""

#: ../../source/authoritiespreferences.rst:8
msgid ""
"*Get there:* More > Administration > Global system preferences > Authorities"
msgstr ""

#: ../../source/authoritiespreferences.rst:14
msgid "General"
msgstr ""

#: ../../source/authoritiespreferences.rst:19
msgid "AuthDisplayHierarchy"
msgstr ""

#: ../../source/authoritiespreferences.rst:21
msgid ""
"Asks: \\_\\_\\_ broader term/narrower term hierarchies when viewing "
"authorities."
msgstr ""

#: ../../source/authoritiespreferences.rst:23
msgid "Default: Don't show"
msgstr ""

#: ../../source/authoritiespreferences.rst:25
#: ../../source/authoritiespreferences.rst:145
#: ../../source/authoritiespreferences.rst:160
#: ../../source/authoritiespreferences.rst:192
#: ../../source/authoritiespreferences.rst:258
#: ../../source/authoritiespreferences.rst:303
#: ../../source/authoritiespreferences.rst:333
#: ../../source/authoritiespreferences.rst:355
#: ../../source/authoritiespreferences.rst:425
msgid "Values:"
msgstr ""

#: ../../source/authoritiespreferences.rst:27
msgid "Don't show"
msgstr ""

#: ../../source/authoritiespreferences.rst:29
msgid "Show"
msgstr ""

#: ../../source/authoritiespreferences.rst:31
#: ../../source/authoritiespreferences.rst:98
#: ../../source/authoritiespreferences.rst:121
#: ../../source/authoritiespreferences.rst:171
#: ../../source/authoritiespreferences.rst:221
#: ../../source/authoritiespreferences.rst:242
#: ../../source/authoritiespreferences.rst:270
#: ../../source/authoritiespreferences.rst:309
#: ../../source/authoritiespreferences.rst:339
#: ../../source/authoritiespreferences.rst:377
#: ../../source/authoritiespreferences.rst:394
#: ../../source/authoritiespreferences.rst:431
msgid "Description:"
msgstr ""

#: ../../source/authoritiespreferences.rst:33
msgid ""
"When set to 'Show' broader and narrower 'See also' references will be "
"displayed in a tree at the top of the record in both the staff interface and "
"the OPAC."
msgstr ""

#: ../../source/authoritiespreferences.rst:37
msgid "|AuthDisplayHierarchy|"
msgstr ""

#: ../../source/authoritiespreferences.rst:39
msgid ""
"For MARC21, in order for this display to appear, the records must have 5XX "
"with the relationship code in $w and the authority number (authid) in $9."
msgstr ""

#: ../../source/authoritiespreferences.rst:44
msgid ""
"The relationship codes in $w can be found on the Library of Congress website "
"under `Tracings and references-General information <https://www.loc.gov/marc/"
"authority/adtracing.html>`_"
msgstr ""

#: ../../source/authoritiespreferences.rst:47
msgid "g - Broader term"
msgstr ""

#: ../../source/authoritiespreferences.rst:49
msgid "h - Narrower term"
msgstr ""

#: ../../source/authoritiespreferences.rst:51
msgid ""
"For example, the records in the above screenshot have the following "
"information::"
msgstr ""

#: ../../source/authoritiespreferences.rst:80
msgid ""
"The relationships must be bidirectional, meaning the broader term must "
"reference the narrower term and the narrower term must reference the broader "
"term."
msgstr ""

#: ../../source/authoritiespreferences.rst:87
msgid "AuthorityControlledIndicators"
msgstr ""

#: ../../source/authoritiespreferences.rst:89
msgid ""
"Asks: Use the following text to edit how authority records control "
"indicators of attached biblio fields (and possibly subfield $2). Lines "
"starting with a comment symbol (#) are skipped. Each line should be of the "
"form: (marc21|unimarc), tag, ind1:(auth1|auth2|some_value), ind2:(auth1|"
"auth2|thesaurus|some_value). Here auth1 and auth2 refer to the indicators of "
"the authority record, tag is a biblio field number or an asterisk (*), and "
"some_value is a fixed value (one character). The MARC21 option thesaurus "
"refers to indicators controlled by authority field 008/11 and 040$f."
msgstr ""

#: ../../source/authoritiespreferences.rst:100
msgid "Used when merging authorities."
msgstr ""

#: ../../source/authoritiespreferences.rst:102
msgid ""
"Controls how the indicators of linked authority records affect the "
"corresponding biblio indicators. Currently, the default value is fine-tuned "
"for MARC21 and copies the authority indicators for UNIMARC."
msgstr ""

#: ../../source/authoritiespreferences.rst:106
msgid ""
"For example, a MARC21 field 100 in a biblio record should pick its first "
"indicator from the linked authority record. The second indicator is not "
"controlled by the authority. This report supports such MARC conventions."
msgstr ""

#: ../../source/authoritiespreferences.rst:113
msgid "AuthorityMergeLimit"
msgstr ""

#: ../../source/authoritiespreferences.rst:115
msgid ""
"Asks: When modifying an authority record, do not update attached "
"bibliographic records if the number exceeds \\_\\_\\_ records. (Above this "
"limit, the :ref:`merge_authority cron job<cron-update-authorities-label>` "
"will update them.)"
msgstr ""

#: ../../source/authoritiespreferences.rst:119
msgid "Default: 50"
msgstr ""

#: ../../source/authoritiespreferences.rst:123
msgid ""
"This system preference determines the maximum number of bibliographic "
"records that can be updated when an authority record changes."
msgstr ""

#: ../../source/authoritiespreferences.rst:126
msgid ""
"This helps prevent overusing resources if an authority record is linked to "
"many bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:129
msgid ""
"Make sure to enable the :ref:`merge_authority cron job<cron-update-"
"authorities-label>` to catch the updates that wouldn't otherwise be "
"transferred to bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:136
msgid "AuthorityMergeMode"
msgstr ""

#: ../../source/authoritiespreferences.rst:138
msgid "Default: loose"
msgstr ""

#: ../../source/authoritiespreferences.rst:140
msgid ""
"Asks: When updating biblio records from an attached authority record "
"(\"merging\"), handle subfields of relevant biblio record fields in \\_\\_"
"\\_ mode. In strict mode subfields that are not found in the authority "
"record, are deleted. Loose mode will keep them. Loose mode is the historical "
"behavior and still the default."
msgstr ""

#: ../../source/authoritiespreferences.rst:147
msgid "loose"
msgstr ""

#: ../../source/authoritiespreferences.rst:149
msgid "strict"
msgstr ""

#: ../../source/authoritiespreferences.rst:154
msgid "AutoCreateAuthorities"
msgstr ""

#: ../../source/authoritiespreferences.rst:156
msgid "Default: do not generate"
msgstr ""

#: ../../source/authoritiespreferences.rst:158
msgid ""
"Asks: When editing records, \\_\\_\\_ authority records that are missing."
msgstr ""

#: ../../source/authoritiespreferences.rst:162
msgid "do not generate"
msgstr ""

#: ../../source/authoritiespreferences.rst:164
msgid "generate"
msgstr ""

#: ../../source/authoritiespreferences.rst:166
#: ../../source/authoritiespreferences.rst:215
#: ../../source/authoritiespreferences.rst:236
#: ../../source/authoritiespreferences.rst:389
msgid "**Important**"
msgstr ""

#: ../../source/authoritiespreferences.rst:168
msgid ""
"`BiblioAddsAuthorities <#biblioaddsauthorities>`__ must be set to \"allow\" "
"for this to have any effect"
msgstr ""

#: ../../source/authoritiespreferences.rst:173
msgid ""
"When this and `BiblioAddsAuthorities <#biblioaddsauthorities>`__ are both "
"turned on, automatically create authority records for headings that don't "
"have any authority link when cataloging. When BiblioAddsAuthorities is on "
"and AutoCreateAuthorities is turned off, do not automatically generate "
"authority records, but allow the user to enter headings that don't match an "
"existing authority. When BiblioAddsAuthorities is off, this has no effect."
msgstr ""

#: ../../source/authoritiespreferences.rst:184
msgid "BiblioAddsAuthorities"
msgstr ""

#: ../../source/authoritiespreferences.rst:186
msgid "Default: allow"
msgstr ""

#: ../../source/authoritiespreferences.rst:188
msgid ""
"Asks: When editing records, \\_\\_\\_ them to automatically create new "
"authority records if needed, rather than having to reference existing "
"authorities."
msgstr ""

#: ../../source/authoritiespreferences.rst:194
msgid "allow"
msgstr ""

#: ../../source/authoritiespreferences.rst:196
msgid ""
"This setting allows you to type values in the fields controlled by "
"authorities and then adds a new authority if one does not exist"
msgstr ""

#: ../../source/authoritiespreferences.rst:199
msgid "don't allow"
msgstr ""

#: ../../source/authoritiespreferences.rst:201
msgid ""
"This setting will lock the authority controlled fields, forcing you to "
"search for an authority versus allowing you to type the information in "
"yourself."
msgstr ""

#: ../../source/authoritiespreferences.rst:208
msgid "MARCAuthorityControlField008"
msgstr ""

#: ../../source/authoritiespreferences.rst:210
msgid "Default: \\|\\| aca\\|\\|aabn \\| a\\|a d"
msgstr ""

#: ../../source/authoritiespreferences.rst:212
msgid ""
"Asks: Use the following text for the contents of MARC21 authority control "
"field 008 position 06-39 (fixed length data elements)."
msgstr ""

#: ../../source/authoritiespreferences.rst:217
msgid ""
"Do not include the date (position 00-05) in this preference, Koha will "
"calculate automatically and put that in before the values in this preference."
msgstr ""

#: ../../source/authoritiespreferences.rst:223
msgid ""
"This preference controls the default value in the 008 field on Authority "
"records. It does not effect bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:229
msgid "UNIMARCAuthorityField100"
msgstr ""

#: ../../source/authoritiespreferences.rst:231
msgid "Default: afrey50 ba0"
msgstr ""

#: ../../source/authoritiespreferences.rst:233
msgid ""
"Asks: Use the following text for the contents of UNIMARC authority field 100 "
"position (fixed length data elements)."
msgstr ""

#: ../../source/authoritiespreferences.rst:238
msgid ""
"Do not include the date (position 00-07) in this preference, Koha will "
"calculate automatically and put that in before the values in this preference."
msgstr ""

#: ../../source/authoritiespreferences.rst:244
msgid ""
"This preference controls the default value in the 100 field on Authority "
"records cataloged in UNIMARC. It does not effect bibliographic records."
msgstr ""

#: ../../source/authoritiespreferences.rst:251
msgid "UseAuthoritiesForTracings"
msgstr ""

#: ../../source/authoritiespreferences.rst:253
msgid "Default: Don't use"
msgstr ""

#: ../../source/authoritiespreferences.rst:255
msgid ""
"Asks: \\_\\_\\_ authority record numbers instead of text strings for "
"searches from subject tracings."
msgstr ""

#: ../../source/authoritiespreferences.rst:260
msgid "Don't use"
msgstr ""

#: ../../source/authoritiespreferences.rst:262
msgid ""
"Search links look for subject/author keywords (example: opac-search.pl?q=su:"
"Business%20networks)"
msgstr ""

#: ../../source/authoritiespreferences.rst:265
msgid "Use"
msgstr ""

#: ../../source/authoritiespreferences.rst:267
msgid ""
"Search links look for an authority record (example: opac-search.pl?q=an:354)"
msgstr ""

#: ../../source/authoritiespreferences.rst:272
msgid ""
"For libraries that have authority files, they may want to make it so that "
"when a link to an authorized subject or author is clicked on the OPAC or "
"staff client it takes the searcher only to a list of results with that "
"authority record. Most libraries do not have complete authority files and so "
"setting this preference to 'Don't use' will allow searchers to click on "
"links to authors and subject headings and perform a keyword search against "
"those fields, finding all possible relevant results instead."
msgstr ""

#: ../../source/authoritiespreferences.rst:284
msgid "Linker"
msgstr ""

#: ../../source/authoritiespreferences.rst:286
msgid ""
"These preferences will control how Koha links bibliographic records to "
"authority records. All bibliographic records added to Koha after these "
"preferences are set will link automatically to authority records, for "
"records added before these preferences are set there is a script (misc/link"
"\\_bibs\\_to\\_authorities.pl) that your system administrator can run to "
"link records together."
msgstr ""

#: ../../source/authoritiespreferences.rst:296
msgid "CatalogModuleRelink"
msgstr ""

#: ../../source/authoritiespreferences.rst:298
#: ../../source/authoritiespreferences.rst:328
msgid "Default: Do not"
msgstr ""

#: ../../source/authoritiespreferences.rst:300
msgid ""
"Asks: \\_\\_\\_ automatically relink headings that have previously been "
"linked when saving records in the cataloging module."
msgstr ""

#: ../../source/authoritiespreferences.rst:305
#: ../../source/authoritiespreferences.rst:335
#: ../../source/authoritiespreferences.rst:427
msgid "Do"
msgstr ""

#: ../../source/authoritiespreferences.rst:307
#: ../../source/authoritiespreferences.rst:337
#: ../../source/authoritiespreferences.rst:429
msgid "Do not"
msgstr ""

#: ../../source/authoritiespreferences.rst:311
msgid ""
"Longtime users of Koha don't expect the authority and bib records to link "
"consistently. This preference makes it possible to disable authority "
"relinking in the cataloging module only (i.e. relinking is still possible if "
"link\\_bibs\\_to\\_authorities.pl is run). Note that though the default "
"behavior matches the previous behavior of Koha (retaining links to outdated "
"authority records), it does not match the intended behavior (updating biblio/"
"authority link after bibliographic record is edited). Libraries that want "
"the intended behavior of authority control rather than the way Koha used to "
"handle linking should set CatalogModuleRelink to 'Do'. Once setting this to "
"'Do' the following preferences can also be set."
msgstr ""

#: ../../source/authoritiespreferences.rst:326
msgid "LinkerKeepStale"
msgstr ""

#: ../../source/authoritiespreferences.rst:330
msgid ""
"Asks: \\_\\_\\_ keep existing links to authority records for headings where "
"the linker is unable to find a match."
msgstr ""

#: ../../source/authoritiespreferences.rst:341
msgid ""
"When set to 'Do', the linker will never remove a link to an authority "
"record, though, depending on the value of :ref:`LinkerRelink <LinkerRelink-"
"label>`, it may change the link."
msgstr ""

#: ../../source/authoritiespreferences.rst:348
msgid "LinkerModule"
msgstr ""

#: ../../source/authoritiespreferences.rst:350
msgid "Default: Default"
msgstr ""

#: ../../source/authoritiespreferences.rst:352
msgid ""
"Asks: Use the \\_\\_\\_ linker module for matching headings to authority "
"records."
msgstr ""

#: ../../source/authoritiespreferences.rst:357
msgid "Default"
msgstr ""

#: ../../source/authoritiespreferences.rst:359
msgid ""
"Retains Koha's previous behavior of only creating links when there is an "
"exact match to one and only one authority record; if the :ref:`LinkerOptions "
"<LinkerOptions-label>` preference is set to 'broader\\_headings', it will "
"try to link headings to authority records for broader headings by removing "
"subfields from the end of the heading"
msgstr ""

#: ../../source/authoritiespreferences.rst:366
msgid "First match"
msgstr ""

#: ../../source/authoritiespreferences.rst:368
msgid ""
"Creates a link to the first authority record that matches a given heading, "
"even if there is more than one authority record that matches"
msgstr ""

#: ../../source/authoritiespreferences.rst:372
msgid "Last match"
msgstr ""

#: ../../source/authoritiespreferences.rst:374
msgid ""
"Creates a link to the last authority record that matches a given heading, "
"even if there is more than one record that matches"
msgstr ""

#: ../../source/authoritiespreferences.rst:379
msgid ""
"This preference tells Koha which match to use when searching for authority "
"matches when saving a record."
msgstr ""

#: ../../source/authoritiespreferences.rst:385
msgid "LinkerOptions"
msgstr ""

#: ../../source/authoritiespreferences.rst:387
msgid "Asks: Set the following options for the authority linker \\_\\_\\_"
msgstr ""

#: ../../source/authoritiespreferences.rst:391
msgid ""
"This feature is experimental and shouldn't be used in a production "
"environment until further expanded upon."
msgstr ""

#: ../../source/authoritiespreferences.rst:396
msgid ""
"This is a pipe separated (\\|) list of options. At the moment, the only "
"option available is \"broader\\_headings.\" With this option set to \"broader"
"\\_headings\", the linker will try to match the following heading as follows:"
msgstr ""

#: ../../source/authoritiespreferences.rst:405
msgid ""
"First: Camins-Esakov, Jared--Coin collections--Catalogs--Early works to 1800"
msgstr ""

#: ../../source/authoritiespreferences.rst:408
msgid "Next: Camins-Esakov, Jared--Coin collections--Catalogs"
msgstr ""

#: ../../source/authoritiespreferences.rst:410
msgid "Next: Camins-Esakov, Jared--Coin collections"
msgstr ""

#: ../../source/authoritiespreferences.rst:412
msgid ""
"Next: Camins-Esakov, Jared (matches! if a previous attempt had matched, it "
"would not have tried this)"
msgstr ""

#: ../../source/authoritiespreferences.rst:418
msgid "LinkerRelink"
msgstr ""

#: ../../source/authoritiespreferences.rst:420
msgid "Default: Do"
msgstr ""

#: ../../source/authoritiespreferences.rst:422
msgid ""
"Asks: \\_\\_\\_ relink headings that have previously been linked to "
"authority records."
msgstr ""

#: ../../source/authoritiespreferences.rst:433
msgid ""
"When set to 'Do', the linker will confirm the links for headings that have "
"previously been linked to an authority record when it runs, correcting any "
"incorrect links it may find. When set to 'Do not', any heading with an "
"existing link will be ignored, even if the existing link is invalid or "
"incorrect."
msgstr ""

#~ msgid "|image14|"
#~ msgstr "|image14|"
