# Compendium of tr.
msgid ""
msgstr ""
"Project-Id-Version: Koha Manual 22.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-12 07:29-0300\n"
"PO-Revision-Date: 2018-10-05 07:29+0000\n"
"Last-Translator: Onur Erdem <onurerdem@sdu.edu.tr>\n"
"Language-Team: Koha Translation Team\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.8\n"
"X-Pootle-Path: /tr/manual22.11/plugins.po\n"
"X-Pootle-Revision: 1\n"
"X-POOTLE-MTIME: 1538724594.243507\n"

#: ../../source/plugins.rst:6
#, fuzzy
msgid "Plugin system"
msgstr "Eklenti Sistemi"

#: ../../source/plugins.rst:8
msgid ""
"Koha's Plugin System allows for you to add additional tools and reports to "
"Koha that are specific to your library. Plugins are installed by uploading "
"KPZ ( Koha Plugin Zip ) packages. A KPZ file is just a zip file containing "
"the perl files, template files, and any other files necessary to make the "
"plugin work."
msgstr ""
"Koha Eklenti Sistemi, Koha’ya kütüphanenize özel olan ek araçlar ve raporlar "
"eklemenize imkan verir. Eklentiler KPZ (Koha Eklenti Zipi) paketleri "
"yüklenerek kurulur. KPZ dosyası, eklentinin çalışabilmesi için oluşturulan, "
"perl dosyaları, şablon dosyaları ve diğer gerekli dosyaları içeren bir zip "
"dosyasıdır."

#: ../../source/plugins.rst:14
msgid "The plugin system needs to be turned on by a system administrator."
msgstr "Eklenti sisteminin bir sistem yöneticisi tarafından açılması gerekir."

#: ../../source/plugins.rst:19
msgid "Set up"
msgstr "Ayarla"

#: ../../source/plugins.rst:21
msgid ""
"To set up the Koha plugin system you must first make some changes to your "
"install."
msgstr ""
"Koha eklenti sistemini kurmak için öncelikle yükleme işleminizde bazı "
"değişiklikler yapmalısınız."

#: ../../source/plugins.rst:24
msgid ""
"Change <enable\\_plugins>0</enable\\_plugins> to <enable\\_plugins>1</enable"
"\\_plugins> in your koha-conf.xml file"
msgstr ""
"koha-conf.xml dosyasında <enable\\_plugins>0</enable\\_plugins> komut "
"dizinini <enable\\_plugins>1</enable\\_plugins> in your koha-conf.xml file "
"olarak değiştirin"

#: ../../source/plugins.rst:27
msgid "Restart your webserver"
msgstr "Web sunucunuzu yeniden başlatın"

#: ../../source/plugins.rst:29
#, fuzzy
msgid ""
"On the Tools page you will see the Tools Plugins and on the Reports page you "
"will see the Reports Plugins."
msgstr ""
"Kurulum tamamlandıktan sonra :ref:`UseKohaPlugins` sistem tercihinizi "
"değiştirmeniz gerekir. Araçlar sayfasında Araçlar Eklentisini, Raporlar "
"sayfasında ise Raporlar Eklentisini göreceksiniz."

#~ msgid "Search History"
#~ msgstr "Arama Geçmişi"

#~ msgid ""
#~ "If you have your :ref:`EnableSearchHistory` preference set to keep your "
#~ "search history then you can access this information by clicking on your "
#~ "username in the top right of the staff client and choosing 'Search "
#~ "history'."
#~ msgstr ""
#~ ":ref:`EnableSearchHistory` tercihini arama geçmişini muhafaza edecek "
#~ "şekilde ayarladıysanız bu bilgiye personel istemcide sağ üstteki "
#~ "kullanıcı adınızı tıklayarak ve 'Arama geçmişi' seçeneğini tıklayarak "
#~ "ulaşabilirsiniz."

#~ msgid "|image1062|"
#~ msgstr "|image1062|"

#~ msgid "From this page you will see your bibliographic search history"
#~ msgstr "Bu sayfadan bibliyografik arama geçmişinizi görebilirsiniz"

#~ msgid "|image1063|"
#~ msgstr "|image1063|"

#~ msgid "And your authority search history."
#~ msgstr "Ve otorite arama geçmişiniz."

#~ msgid "|image1064|"
#~ msgstr "|image1064|"

#~ msgid "About Koha"
#~ msgstr "Koha Hakkında"

#~ msgid ""
#~ "The 'About Koha' area will give you important server information as well "
#~ "as general information about Koha."
#~ msgstr ""
#~ "'Koha Hakkında' alanı, size önemli sunucu bilgilerinin yanı sıra Koha "
#~ "hakkında genel bilgi verecektir."

#~ msgid "*Get there:* More > About Koha"
#~ msgstr "*Gidiş yolu:* Daha fazla > Koha Hakkında"

#~ msgid "Server Information"
#~ msgstr "Sunucu Bilgisi"

#~ msgid ""
#~ "Under the 'Server Information' tab you will find information about the "
#~ "Koha version and the machine you have installed Koha on. This information "
#~ "is very important for debugging problems. When reporting issues to your "
#~ "support provider or to the various other support avenues (mailing lists, "
#~ "chat room, etc), it's always good to give the information from this "
#~ "screen. The time zone is handled via Koha or server configuration. For "
#~ "more information how to specify time zones per instance, see https://wiki."
#~ "koha-community.org/wiki/Time_Zone_Configuration"
#~ msgstr ""
#~ "'Sunucu Bilgileri' sekmesi altında Koha sürümü ve Koha'yı kurduğunuz "
#~ "makine hakkında bilgi bulabilirsiniz. Bu bilgi, hata ayıklama sorunları "
#~ "için çok önemlidir. Sorunları destek sağlayıcınıza veya diğer çeşitli "
#~ "destek yollarına (posta listeleri, sohbet odası vb.) Bildirirken, bu "
#~ "ekrandan bilgi vermek her zaman iyidir. Saat dilimi Koha veya sunucu "
#~ "yapılandırması ile işlenir. Örnek başına saat dilimlerinin nasıl "
#~ "belirtildiğine daha fazla bilgi için, bkz. https://wiki.koha-community."
#~ "org/wiki/Time_Zone_Configuration"

#~ msgid "|image1065|"
#~ msgstr "|image1065|"

#~ msgid "Perl Modules"
#~ msgstr "Perl Modülleri"

#~ msgid ""
#~ "In order to take advantage of all of the functionalities of Koha, you "
#~ "will need to keep your Perl modules up to date. The 'Perl Modules' tab "
#~ "will show you all of the modules required by Koha, the version you have "
#~ "installed and whether you need to upgrade certain modules."
#~ msgstr ""
#~ "Koha'nın işlevselliğinden tam olarak yararlanmanız için Perl "
#~ "modüllerinizi güncel tutmanız gerekir. 'Perl Modülleri' sekmesi size "
#~ "Koha'nın gerek duyduğu tüm modülleri, kurmuş olduğunuz sürüm ve belirli "
#~ "modüller üzerinde bir güncelleştirmenin gerekli olup olmadığını "
#~ "gösterecektir."

#~ msgid "|image1066|"
#~ msgstr "|image1066|"

#~ msgid ""
#~ "Items listed in bold are required by Koha, items highlighted in red are "
#~ "missing completely and items highlighted in yellow simply need to be "
#~ "upgraded."
#~ msgstr ""
#~ "Kalın olarak listelenen materyaller Koha tarafından gereklidir, "
#~ "kırmızıyla vurgulanan materyaller tamamen eksiktir ve sarı renkle "
#~ "vurgulanan materyallerin yalnızca güncellenmesi gerekir."

#~ msgid "System Information"
#~ msgstr "Sistem Bilgisi"

#~ msgid ""
#~ "This tab will provide you with warnings if you are using system "
#~ "preferences that have since been deprecated or system preferences that "
#~ "you have set without other required preferences"
#~ msgstr ""
#~ "Artık kullanılmayan ya da gerekli diğer tercihler olmadan ayarladığınız "
#~ "sistem tercihlerini kullanıyorsanız, bu sekme size uyarı verecektir"

#~ msgid "|image1067|"
#~ msgstr "|image1067|"
